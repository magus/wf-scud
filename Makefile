.PHONY: build setup

build:
	ninja -C _build

setup:
	meson _build .
	ln -s _build/compile_commands.json .
