#include <wayfire/core.hpp>
#include <wayfire/output.hpp>
#include <wayfire/plugin.hpp>
#include <wayfire/signal-definitions.hpp>
#include <wayfire/util/log.hpp>
#include <wayfire/view.hpp>
#include <wayfire/workspace-manager.hpp>

#include "view_list.hpp"

namespace {
enum class ViewSize { small, medium, large };
struct ViewData {
    ViewSize size;
    bool maximized;
};

void fit_view_in_workarea(wf::geometry_t &view_geom, wf::geometry_t wkarea_geom);
double size_to_multiplier(ViewSize sz);
ViewSize next_size(ViewSize sz);
std::pair<int, int> point_to_pair(const wf::point_t &pt);
} // namespace

class scud_t : public wf::plugin_interface_t {
    wf::option_wrapper_t<wf::keybinding_t> key_center_view{"scud/key_center"};
    wf::option_wrapper_t<wf::keybinding_t> key_move_left{"scud/key_move_left"};
    wf::option_wrapper_t<wf::keybinding_t> key_move_right{"scud/key_move_right"};
    wf::option_wrapper_t<wf::keybinding_t> key_swap_left{"scud/key_swap_left"};
    wf::option_wrapper_t<wf::keybinding_t> key_swap_right{"scud/key_swap_right"};
    wf::option_wrapper_t<wf::keybinding_t> key_cycle_size{"scud/key_cycle_size"};
    wf::option_wrapper_t<wf::keybinding_t> key_toggle_maximized{"scud/key_toggle_maximized"};
    std::map<std::pair<int, int>, view_list_t> views;
    std::map<wayfire_view, ViewData> view_data;

    wf::signal_connection_t workarea_changed_cb = [=](wf::signal_data_t *) {
        auto ws = point_to_pair(output->workspace->get_current_workspace());
        if (views.count(ws) == 0) {
            views[ws] = view_list_t{};
        }
        output->focus_view(views[ws].active);
    };

    wf::signal_connection_t workspace_changed_cb = [=](wf::signal_data_t *data) {
        auto ev = (wf::workspace_changed_signal *)(data);

        auto ws = point_to_pair(ev->new_viewport);
        if (views.count(ws) == 0) {
            views[ws] = view_list_t{};
        }
        output->focus_view(views[ws].active);
    };

    wf::signal_connection_t created_cb = [=](wf::signal_data_t *data) {
        auto ev = (wf::view_mapped_signal *)(data);
        auto view = get_signaled_view(data);

        // TODO: this is from the place plugin, is it good?
        if ((view->role != wf::VIEW_ROLE_TOPLEVEL) || view->parent || view->fullscreen ||
            view->tiled_edges || ev->is_positioned) {
            return;
        }

        auto ws = point_to_pair(output->workspace->get_current_workspace());
        auto workarea = output->workspace->get_workarea();

        wf::geometry_t geom{views[ws].next_view_xpos(), workarea.y,
                            int(workarea.width * size_to_multiplier(ViewSize::small)),
                            workarea.height};
        fit_view_in_workarea(geom, workarea);

        views[ws].add_in_focus(view);
        view_data[view] = {ViewSize::small, false};
        views[ws].active->set_geometry(geom);
        views[ws].adjust_geometries();

        ev->is_positioned = true;
    };

    wf::signal_connection_t destroyed_cb = [=](wf::signal_data_t *data) {
        auto view = get_signaled_view(data);
        auto ws = point_to_pair(output->workspace->get_current_workspace());

        views[ws].remove(view);
        view_data.erase(view);
        if (views[ws].active != nullptr) {
            auto workarea = output->workspace->get_workarea();
            auto geom = views[ws].active->get_wm_geometry();
            fit_view_in_workarea(geom, workarea);
            views[ws].active->set_geometry(geom);
            views[ws].adjust_geometries();
            output->focus_view(views[ws].active);
        }
    };

    wf::key_callback on_center_view = [=](auto) {
        auto ws = point_to_pair(output->workspace->get_current_workspace());

        if (views[ws].active != nullptr) {
            auto workarea = output->workspace->get_workarea();
            auto geom = views[ws].active->get_wm_geometry();
            geom.x = workarea.width * .5 - geom.width * .5;
            fit_view_in_workarea(geom, workarea);
            views[ws].active->set_geometry(geom);
            views[ws].adjust_geometries();

            return true;
        } else {
            return false;
        }
    };

    wf::key_callback on_move_left = [=](auto) {
        auto ws = point_to_pair(output->workspace->get_current_workspace());

        if (views[ws].move_left()) {
            auto workarea = output->workspace->get_workarea();
            auto geom = views[ws].active->get_wm_geometry();
            fit_view_in_workarea(geom, workarea);
            views[ws].active->set_geometry(geom);
            views[ws].adjust_geometries();
            output->focus_view(views[ws].active);
        }
        return true;
    };

    wf::key_callback on_move_right = [=](auto) {
        auto ws = point_to_pair(output->workspace->get_current_workspace());

        if (views[ws].move_right()) {
            auto workarea = output->workspace->get_workarea();
            auto geom = views[ws].active->get_wm_geometry();
            fit_view_in_workarea(geom, workarea);
            views[ws].active->set_geometry(geom);
            views[ws].adjust_geometries();
            output->focus_view(views[ws].active);
        }
        return true;
    };

    wf::key_callback on_swap_left = [=](auto) {
        auto ws = point_to_pair(output->workspace->get_current_workspace());
        auto res = views[ws].swap_left();

        if (res.first) {
            auto workarea = output->workspace->get_workarea();
            auto geom = views[ws].active->get_wm_geometry();
            geom.x = res.second;
            fit_view_in_workarea(geom, workarea);
            views[ws].active->set_geometry(geom);
            views[ws].adjust_geometries();
        }
        return true;
    };

    wf::key_callback on_swap_right = [=](auto) {
        auto ws = point_to_pair(output->workspace->get_current_workspace());
        auto res = views[ws].swap_right();

        if (res.first) {
            auto workarea = output->workspace->get_workarea();
            auto geom = views[ws].active->get_wm_geometry();
            geom.x = res.second;
            fit_view_in_workarea(geom, workarea);
            views[ws].active->set_geometry(geom);
            views[ws].adjust_geometries();
        }
        return true;
    };

    wf::key_callback on_cycle_size = [=](auto) {
        auto ws = point_to_pair(output->workspace->get_current_workspace());

        if (views[ws].active == nullptr)
            return true;

        view_data[views[ws].active].size = next_size(view_data[views[ws].active].size);

        auto workarea = output->workspace->get_workarea();
        auto geom = views[ws].active->get_wm_geometry();
        geom.width = workarea.width * size_to_multiplier(view_data[views[ws].active].size);
        fit_view_in_workarea(geom, workarea);
        views[ws].active->set_geometry(geom);
        views[ws].adjust_geometries(geom);

        return true;
    };

    wf::key_callback on_toggle_maximized = [=](auto) {
        auto ws = point_to_pair(output->workspace->get_current_workspace());

        if (views[ws].active == nullptr)
            return true;

        view_data[views[ws].active].maximized ^= true;
        auto workarea = output->workspace->get_workarea();
        auto geom = workarea;

        if (!view_data[views[ws].active].maximized) {
            geom.width = workarea.width * size_to_multiplier(view_data[views[ws].active].size);
            fit_view_in_workarea(geom, workarea);
        }
        views[ws].active->set_geometry(geom);
        views[ws].adjust_geometries(geom);

        return true;
    };

  public:
    void init() override {
        LOGI("Initializing scud");

        // Are these the signals I need?
        // https://github.com/WayfireWM/wayfire/blob/e1e0ce383044c5fc0b0802e01d59dc6a3c6bd244/plugins/single_plugins/place.cpp#L60
        output->connect_signal("workarea-changed", &workarea_changed_cb);
        output->connect_signal("workspace-changed", &workspace_changed_cb);
        output->connect_signal("view-mapped", &created_cb);
        output->connect_signal("view-unmapped", &destroyed_cb);

        output->add_key(key_center_view, &on_center_view);
        output->add_key(key_move_left, &on_move_left);
        output->add_key(key_move_right, &on_move_right);
        output->add_key(key_swap_left, &on_swap_left);
        output->add_key(key_swap_right, &on_swap_right);
        output->add_key(key_cycle_size, &on_cycle_size);
        output->add_key(key_toggle_maximized, &on_toggle_maximized);
    }

    void fini() override {}
};

namespace {
void fit_view_in_workarea(wf::geometry_t &view_geom, wf::geometry_t wkarea_geom) {
    // TODO: adjust for multiple workareas
    view_geom.x = std::max(view_geom.x, wkarea_geom.x);
    view_geom.y = wkarea_geom.y;
    view_geom.width = std::min(view_geom.width, wkarea_geom.width);
    view_geom.height = std::min(view_geom.height, wkarea_geom.height);
    if (view_geom.x + view_geom.width > wkarea_geom.width) {
        view_geom.x = wkarea_geom.width - view_geom.width;
    }
}

ViewSize next_size(ViewSize sz) {
    switch (sz) {
    case ViewSize::small:
        return ViewSize::medium;
    case ViewSize::medium:
        return ViewSize::large;
    case ViewSize::large:
        return ViewSize::small;
    }
    return ViewSize::small;
}

double size_to_multiplier(ViewSize sz) {
    switch (sz) {
    case ViewSize::small:
        return .4;
    case ViewSize::medium:
        return .5;
    case ViewSize::large:
        return .6;
    }
    return .4;
}

std::pair<int, int> point_to_pair(const wf::point_t &pt) { return {pt.x, pt.y}; }
} // namespace

DECLARE_WAYFIRE_PLUGIN(scud_t)
