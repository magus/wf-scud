#ifndef VIEW_LIST_HPP
#define VIEW_LIST_HPP

#include <list>
#include <map>
#include <wayfire/util/log.hpp>
#include <wayfire/view.hpp>

struct view_list_t {
    // The view that currently has focus.
    wayfire_view active;

    // The views to the left of the focus.
    std::list<wayfire_view> lefts;

    // The views to the right of the focus.
    std::list<wayfire_view> rights;

    int next_view_xpos() {
        if (active == nullptr)
            return 0;

        auto g = active->get_wm_geometry();

        return (g.x + g.width);
    }

    // Add a view, in focus.
    void add_in_focus(wayfire_view nw) {
        if (active == nullptr) {
            active = nw;
        } else {
            lefts.push_front(active);
            active = nw;
        }
    }

    // Remove a view.
    void remove(wayfire_view view) {
        if (view == active && lefts.size() > 0) {
            active = lefts.front();
            lefts.pop_front();
        } else if (view == active && rights.size() > 0) {
            active = rights.front();
            rights.pop_front();
        } else if (view == active) {
            active = nullptr;
        } else {
            lefts.remove(view);
            rights.remove(view);
        }
    }

    // Adjust geometries of all views around the acive view.
    void adjust_geometries() {
        if (active == nullptr)
            return;

        auto g = active->get_wm_geometry();

        for (auto rv : rights) {
            rv->move(g.x + g.width, g.y);
            g = rv->get_wm_geometry();
        }

        g = active->get_wm_geometry();
        for (auto lv : lefts) {
            auto glv = lv->get_wm_geometry();
            lv->move(g.x - glv.width, g.y);
            g = lv->get_wm_geometry();
        }
    }

    // Adjust the geometries of all non-active views around the provided
    // geometry. Sometimes it seems like the size isn't set immediately when
    // calling set_geometry, and then this function is handy.
    void adjust_geometries(wf::geometry_t g) {
        for (auto rv : rights) {
            rv->move(g.x + g.width, g.y);
            g = rv->get_wm_geometry();
        }

        g = active->get_wm_geometry();
        for (auto lv : lefts) {
            auto glv = lv->get_wm_geometry();
            lv->move(g.x - glv.width, g.y);
            g = lv->get_wm_geometry();
        }
    }

    bool move_left() {
        if (lefts.front() == nullptr)
            return false;

        rights.push_front(active);
        active = lefts.front();
        lefts.pop_front();

        return true;
    }

    bool move_right() {
        if (rights.front() == nullptr)
            return false;

        lefts.push_front(active);
        active = rights.front();
        rights.pop_front();

        return true;
    }

    std::pair<bool, int> swap_left() {
        if (lefts.front() == nullptr)
            return {false, 0};

        rights.push_front(lefts.front());
        lefts.pop_front();

        return {true, rights.front()->get_wm_geometry().x};
    }

    std::pair<bool, int> swap_right() {
        if (rights.front() == nullptr)
            return {false, 0};

        lefts.push_front(rights.front());
        rights.pop_front();

        return {true, lefts.front()->get_wm_geometry().x};
    }
};

#endif
