* To run wayfire

#+begin_src shell
WAYFIRE_PLUGIN_PATH=${PWD}/_build/src \
    WAYFIRE_PLUGIN_XML_PATH=${PWD}/metadata \
    wayfire -d -c test-cfg.ini >| output.log
#+end_src
